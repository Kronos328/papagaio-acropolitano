from telegram.ext import Updater, CommandHandler, Filters
import logging
import json
import dice
import os

token = os.getenv("TOKEN")

PORT = int(os.environ.get("PORT", "8443"))

updater = Updater(token=token, use_context=True)

dispatcher = updater.dispatcher

handlers = {}

frases_dostoiovo = json.loads(open("resources/dostoiovo.json", "r").read())

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)


def start(update, context):
    context.bot.send_message(chat_id=update.effective_chat.id, text="Em verdade, em verdade vos digo:\nSe o grão de "
                                                                    "trigo que cair na terra não morrer,\npermenecerá "
                                                                    "só; mas se morrer, \nproduzirá muito fruto."
                                                                    "\nJoão 12:24")


def groll(update, context):
    if len(context.args) == 0:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text='Insira o comando com a estrutura certa: "/groll (nome_da_perícia) (Valor)"',
            reply_to_message_id=update.effective_message.message_id,
        )
        return

    skill_name = context.args[0]
    skill_value = context.args[1]
    roll = dice.roll("3d6")
    message = ""
    roll_sum = 0

    for r in roll:
        roll_sum += r

    try:
        skill_value = int(skill_value)
    except ValueError:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text="Insira um valor numérico para a perícia em questão.",
            reply_to_message_id=update.effective_message.message_id,
        )
        return

    if roll_sum <= skill_value:
        message += "Você passou "
    else:
        message += "Você falhou "

    message += "seu teste de " + skill_name + " com margem " + str((skill_value - roll_sum)) + \
               "\n\n [" + str(roll.random_element.result[0]) + "] + " + \
               "[" + str(roll.random_element.result[1]) + "] + " + \
               "[" + str(roll.random_element.result[2]) + "]" + \
               " => " + str(roll_sum)

    context.bot.send_message(
        reply_to_message_id=update.effective_message.message_id,
        text=message,
        chat_id=update.effective_chat.id

    )


def dostoiovo(update, context):
    roll = dice.roll("1d" + str(len(frases_dostoiovo)))
    frase = frases_dostoiovo[int(roll.evaluate()) - 1]
    context.bot.send_message(
        text=frase,
        chat_id=update.effective_chat.id
    )


def create_handlers():
    handlers["start"] = CommandHandler('start', start, filters=Filters.command)
    handlers["groll"] = CommandHandler('groll', groll, filters=Filters.command)
    handlers["dostoiovo"] = CommandHandler(
        'dostoiovo',
        dostoiovo,
          filters=Filters.command
          )


def add_handlers_to_dispatcher(_dispatcher):
    for handler in handlers:
        _dispatcher.add_handler(handlers[handler])


def main():
    create_handlers()
    add_handlers_to_dispatcher(dispatcher)
    updater.start_webhook(
        listen='0.0.0.0',
        url_path=token,
        webhook_url="https://papagaio-acropolitano.herokuapp.com/" + token,
        port=PORT
    )


if __name__ == '__main__':
    main()
